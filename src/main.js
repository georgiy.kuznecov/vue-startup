require("uikit/dist/css/uikit.css");
import UIkit from "uikit";
import Icons from 'uikit/dist/js/uikit-icons';
UIkit.use(Icons);

import Vue from 'vue';

import Application from './application.vue';
import Example from './example.vue';

Vue.component('example', Example);

let myVue = new Vue({
  el: '#application',
  render: h => h(Application),
  data: { "test" : "Test title" }
});

